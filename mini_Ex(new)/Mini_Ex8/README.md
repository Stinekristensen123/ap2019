Link: https://cdn.staticaly.com/gl/Stinekristensen123/ap2019/raw/master/mini_Ex(new)/Mini_Ex8/empty-example/index.html


We; Mette, Stine and Sophie, have chosen to work on a Haiku generator, creating longing- and heartfelt, nature poems, through pseudo-random generator on computational devices. 

A haiku, which is a Japanese poem, is usually the intermediary of a theme and a setting. These themes and settings must be described so well that they stand completely sharp for the reader. Often the settings, themes and the nature presented are not obviously related, this can help the reader create their own meaning of the Haiku and interpret the different components of it - eg. forms contrast. 

This in itself is a paradox, and we’ve spent quite some time discussing what the generation progress does to the art-form of creating these Haiku poems. Using a machine as a medium to convey feelings, or to put various lines and rhythms together will possibly create some absurd poems, or lines that we wouldn’t necessarily put together – which we feel conveys the writings and poetry of modern society. It’s not enough to convey your feelings, or pour your heart into a book, the pseudorandom aspect of our code/ poem generator is taking a spin on Haiku, making it more humorous and “random”.

We are using arrays to group lines of the poem, that we’ve written, or taken from various other Haiku books, and generators online.
One group of arrays with 5 syllables, with nature statements
The second group of arrays is with 7 syllables, focused on describing actions and movements. 
The third group of arrays has 5 syllables, as the first one, and the main focus in this group is to have an open ending and be intriguing.

With the background and colour spectrum of the generator, we’ve tried to create an ephemeral look, fragile but coherent – trying to make the poems the centre of attention. Furthermore, the backgrounds’ simplicity serves as a contrast to the very descriptive poems. 
With these choices, we are trying to pay homage to the roots of Haiku, being something beautiful to frame and share with people.

“My thoughts on this weeks mini_ex, was to create some poetry with no deeper meaning. Modern poetry often presents the first thoughts that come to the artist’s mind. I think that is a fun aspect of poetry to explore. I think it was fun to create a Hauki poem, because it made me realise how easy it is to think you can relate to a poem. When generating random poems with our program, I couldn’t help thinking how easy it was to create a poem by using randomness” – Stine 
