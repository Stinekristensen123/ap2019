var one = ["i love summer", "wind is blowing now", "winter is coming", "spring smells like lovers", "love is eternal", "i can feel the fall", "the sun and the moon", "the moon is my friend", "all the leaves turn brown", "the sun is my friend"];
var two = ["covers me in light and warmth", "now black twigs are stretching out", "snow is smelting on te poles", "my iPhone is out of power", "while twisting its way around", "don't hurt me no more", "shining bright like a diamond", "it feels so familiar", "as far as the eye can see", "and i hear a wolf howling"];
var three = ["to go to heaven", "for eternal use", "flying to heaven", "keeping me happy", "a track to follow", "this is the first day", "dancing on alone", "where are the flowers", "am i the nature", "it itches down there"];


function setup() {
  createCanvas(700, 700);
  background(41, 133, 255);

}

function mouseClicked() {
  if (mouseX > 200 && mouseX < 500 && mouseY > 130 && mouseY < 170 ){

    clear();

    background(41, 133, 255);

  push();
    textSize(29);
    textFont('Apple Chancery');
    fill(255);
    text(random(one), 200, 300);
    text(random(two), 200, 370);
    text(random(three), 200, 440);
	pop();

  }
}

function draw() {



push();
 rectMode(CENTER);
  noStroke();
  fill(51, 60, 255);
  rect(350, 150, 300, 40, 11);

  textAlign(CENTER);
  strokeWeight(0);
  textFont('Apple Chancery');
  textSize(16)
  fill(255);
  text("h a i k u   g e n e r a t o r", 350, 155);
pop();


}
