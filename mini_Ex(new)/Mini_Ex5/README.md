[RUNME](https://cdn.staticaly.com/gl/Stinekristensen123/ap2019/raw/master/mini_Ex(new)/Mini_Ex5/empty-example/index.html)

What is the concept of your work? What do you want to express? What have you changed and why?



My first thought with the revisit was to improve my last week’s mini_Ex. In my last week’s mini_Ex my idea was to map all data based on the users media-interactions, and show it to the user. The concepts behind my program, was to get a critical point across the data capturing that is taking place all over the digital media today. 
Last week I had a hard time understanding how to connect a button+text and button+webcam. Furthermore I just wanted to explore the previous learnt basic syntaxes and functions. Syntaxes such as loop, array, push()/pop(), clicking on objects, if statements and the Boolean variables, was something I would like to remember without any help. 
I went back to my last week’s mini_Ex to explore the connection between a button and a text/or webcam. At first I thought that I had to use the if() syntax. I tried out with if(mousePressed=button), if(mousepressed, x1, y1, x2, y2), which actually didn’t make any sense, due to the fact that I had not defined anything. After watching several videos of “The Coding Train”, I realised that I had to create a button, define the button, and connect it with the text by making a function called function text(). Furthermore I used push() and pop() to provide more control while programming. 
Afterwards I played around with loop and array, but I decided to delete the new syntaxes from my program, since they didn’t contribute with anything to the concept of the program.
 

What does it mean by programming as a practice? What is the relation between programming and digital culture?

Computation is an important aspect of our today’s society, therefor programming-language should be thought to everyone in education and everyone with even just a tiny bit of interest for politic, culture and our society. As it is written in the article called “Aesthetic Programming” by Aarhus University: “Together the courses offer ways of thinking about software and computational culture to understand wider political, cultural, social and aesthetic phenomena, and the ways in which our experiences of the world are ever more underscored by computational processing”. Despite from getting a better understanding of culture and media, researchers will be able to improve existing data, methods and theories.- A new world of opportunities. But unfortunately people are not interested in learning to program, because it is easier not to. Fortunately it has become, - and still is, - easier to learn coding in the new programs. 
I also think it should taught in a broader perspective, so that people will keep the control while using their computers. 



![ScreenShot](Skærmbillede 2019-03-11 kl. 23.02.33.png)