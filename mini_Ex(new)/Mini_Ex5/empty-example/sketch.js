
var input
var button
var button1
var button2
var webcam1




function setup() {
	createCanvas(600, 600);

	//creating searchfield
	input = createInput('Type here');
	input.position(80,100);


	//creating "send" button
	button = createButton('Send');
	button.position(200,100)

	//creating "live stream" button
	button1 = createButton('Live stream');
	button1.position(100,250);
	button1.mousePressed(webcam1);


	//Creating "interreseret" button
	button2 = createButton('*Interreseret');
	button2.position(40,450);
	button2.mousePressed(text1);

	//Creating "Deltager" button
	button3 = createButton('Deltager');
	button3.position(40,480);
	button3.mousePressed(text2);


	//Creating "Deltager ikke" button
	button4 = createButton('Deltager ikke');
	button4.position(40,510);
	button4.mousePressed(text3);

}


function draw() {
  background('#FFFFFF');
	fill('#3366FF')
	text (input.value(), 380, 110);



	push()
		stroke('#222222');
		strokeWeight(2);
		line(300, 0, 300, 600);
	pop()


	push()
		stroke('#222222');
		strokeWeight(1);
		line(300,80,600,80);
	pop()



	fill('#3366FF');
	rect(0,0,300,50);




	fill('#FFFFFF');
	rect(300,0,300,50);

textSize(20);
fill('#000033')
text ("Stine Suhr Kristensen", 380,40);



	fill('#FFFFFF')
text("FACEBOOK", 80,40);

	textSize(15)
	fill('#000033')
	text("Information Map", 380,70);

}



//Webcam position and size
	function webcam1() {
		capture= createCapture();
		capture.size(200,200);
		capture.position(400,200);

}

//The text that is connected with button2
function text1(){
  let text1=createDiv("Stine er interreseret i Nickelback koncert");
  text1.position(350,420);
  text1.style('color','#EE82EE');

}


//The text that is connected with button3
function text2(){
  let text2=createDiv("Stine deltager i hackathon");
  text2.position(350,460);
  text2.style('color','#EE82EE');


}

//The text that is connected with button3
function text3(){
  let text3=createDiv("Stine deltager ikke i 'Roskilde 2019'");
  text3.position(350,490);
  text3.style('color','#EE82EE');



}
