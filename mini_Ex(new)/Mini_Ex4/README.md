[RUNME](https://cdn.staticaly.com/gl/Stinekristensen123/ap2019/raw/master/mini_Ex(new)/Mini_Ex4/empty-example/index.html)




   Describe about your sketch and explain what have been captured both conceptually and technically. How's the process of capturing? 

My first thought on this week’s mini_ex was that I wanted to demonstrate how Facebook is capturing all of our information and personal data. Due to the fact that Facebook is capturing our personal information and data, it has got some kind of power. 
After reading the text “The like economy: Social buttons and the data-intensive web”, I realised that Facebook is not only gathering data, but also decides which data websides and so on are able to access- even though it’s produced by themselves. It made me realise how powerfull facebook has become. Therefor I would like to create a program that stressed the fact that Facebook is capturing it all, which people (including myself) often forget. 
My program consists of a Facebook page and a “data/information” map. On the Facebook page I have focused on the fact that whatever you search for or all the messages you sent, will be captured on Facebook. When I am writing in the search field on the Facebook page, whatever I write will appear in the “information” map. The information map symbolizes all the data that Facebook has gathered about me. Everything I do will appear in the map.
Furthermore you can press the button “live stream” and a webcam appears in the “information” map. 


   Together with the assigned reading and coding process, how might this ex helps you to think about or understand the data capturing process in digital culture? 

With the assigned reading and coding process, I have learned what the data capturing process consists of. The different ways of capturing (Heat map, keyboard tracking, buttons, options and even taste. But “CAPTURE ALL” is not only concerning about the data capture, it is also dealing with the concept as a criticism to society. That people should be able to have more opportunities and not putting people in specific boxes with specific labels. 
It has been very interesting to read the assigned reading and achieve an understanding of the concept “CAPTURE ALL”.  

![ScreenShot](Skærmbillede 2019-03-05 kl. 16.18.18.png)

