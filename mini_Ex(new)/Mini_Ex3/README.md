[RUNME](https://cdn.staticaly.com/gl/Stinekristensen123/ap2019/raw/master/mini_exes/mini_ex2%20kopi/index.html)




•	What are the time-related syntaxes/functions that you have used in your program? and why you use in this way? How is time being constructed in computation (can refer to both reading and your process of coding)? 



In my program I have used different time-related syntaxes/functions. First of all I have used “array”, so that I was able to store several variables instead of only one. I used “array” to make a list of different words (a list of data). I wanted the word to change each time the mouse was pressed on the canvas, and they should occur in a particular order. Each word in the array is identified by an index number representing its position in the array, and thereby I sorted the order of the words.
It was difficult to create the text in my, because my throbber is a 3D figure. Therefore I had to download and incorporate a font to run the text. 
I also made use of “conditional statement”, which means the if() function. This function will run, if the statement in the brackets is true or false. I used the function as a kind of loop. When the array of words is done, the index number will go back to 0 and start over again. –The text will change between every mouse press. 

As I said, the time related syntax in my program is a loop, which runs the word in the middle over and over again. I incorporated the text and loop to entertain the user while waiting. The text is from a song called “waiting game” by BANKS, which I always think about when I’m impatiently waiting. 



•  Think about a throbber that you have encounted in digital culture e.g streaming video on YouTube or loading latest feeds on Facebook or waiting a ticket transaction, what do you think a throbber tells us, and/or hides, about? How might we think about this remarkable throbber icon differently


I always hate to see the throbber on my screen. I know that something is working/progressing in my computer, and I have to wait a little while. But nowadays we are used to never being bored and always entertained. We are always entertained by our smartphones, TV, computers etc., which is why we get so easily bored. –Unfortunately.  



![ScreenShot](Skærmbillede 2019-02-25 kl. 19.46.25.png)

