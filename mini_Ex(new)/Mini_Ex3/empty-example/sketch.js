//Throbber:

//myFont bruges at sætte tekst ind(mix af 2D+3D)
var myFont;
var i=0;
//Jeg har gjort brug af array(se l. 38, så ordene kan skifte)
var array=["IT'S","A","WAITING","GAME"];


//For at sætte tekst ind, bruges bla. preload som også bruges til billedindsætning
function preload(){
  myFont = loadFont('AppleLiSungLight.ttf');
}


function setup() {
  createCanvas(600, 600, WEBGL);
}


function draw() {
  background('#74C1D1');

  //Jeg har kommet push+pop om rotate syntaxen, således teksten ikke roterer
  push();
  fill('#605AC8')
  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.01);
  torus(170,10)
  pop();

  //TextFont syntaxen er placeret under drawfunktionen, således canvas ikke overtegner teksten
  push();
  fill(100,100,255)
  textFont(myFont);
  textSize(40);
  //Variablen "i" aktiveres
  text(array[i], -50, 0);
  pop();

  //if syntaxen bruges til at få ordene til at loope. Når der ikke er flere ord, går den tilbage til 0.
  if(i===5){
    i=0;
  }


}

  //Jeg har brugt funktionene mouseClicked, så ordene skifter når man klikker på canvas
  function mouseClicked (){

  i=i+1;


}
