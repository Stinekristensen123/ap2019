**Individual**

Based on my mini_Ex10: [link](https://gitlab.com/Stinekristensen123/ap2019/tree/master/mini_Ex(new)/Mini_Ex9)



I have chosen to visualize my mini_ex10, because it was the one i found most difficult to create, due to the technical aspect of the program. 
I made a flowchart over my program, to make it more simple for a "non-coder" to understand. I wanted to make a flowchart, that could communicate with the user, but also myself. 
It was funny to see how simple my program actually was, eventhough I had a hard time making it. 
I found it difficult to draw the flowchart, when choosing what was important to include. I could either choose to make a flowchart of the technical aspect of the program or the "usage" aspect of it. As mentioned I chose to make a flowchart for the user, and therefore it became very simple. 


![ScreenShot](Flowchart1.jpg)














**Group work**




**Idea 1:**


*The signal value og emojies- truemoji, what do you mean? *

The first idea of a program is based on the topic from week 2 in AP called "Geometric Shapes". This week's topic reffered, among other things, to emojis and the the cultural aspect of emojis. We were discussing what emojis should be used for, and if they should reflect humans. Furthermore the consequences of emojis reflecting humans were discussed. An aspect of the topic we found very interesting, was to look at the signal value of emojis and how it affects our understanding of a message(both sending and recieving)









We have created a prototype of our first idea in Axure: [Link](https://cz17yw.axshare.com/?fbclid=IwAR20aALBr3t883PJnyXFnQU6r4iRDydsSIdXyryCfCXHvalKmT1mIc9GckE#g=1&p=home)














![ScreenShot](Flowchart3.jpg)






What might be the technical challenges for the first idea:

•	To avoid aesthetic disagreements during the coding process, we will make an aesthetical prototype of our program in Axure.

•	Another predicted technical challenge in this design process will occur if we choose option number 2. If we choose option number 2 the output of the program, will show a collage filled with other peoples messages including your own, to make it possible for the user to compare his/hers message and signal value of the message with others. We would like to be able to capture different users messages, and we actually don’t know how to solve this challenge.

•	The last technical challenge of the program, will probably the whole interactive aspect. For example to connect a button with the different emojis, and let them disappear after the user have chosen a specific emoji. 














**Idea 2:** 



*Capmojis – the most including cap app for making emojis of yourself. Express your true feelings; capmojis *


The second idea of a program is based on the same thoughts as those for our first idea. This idea is just a solution of how emojis maybe would not be misunderstood or misinterpreted. 







![ScreenShot](Flowchart 2.jpg)






What might be the technical challenges for the second idea:


•	To shape and make the “emoji” in which your face will show --> how to go about the aesthetic limitations of trying to fit a whole face inside the original emoji format.

•	How to keep the data capture safe, and prevent other users of the app to get hold of your personal smileys. It’s also a matter of copyright? Who has the ownership of the personal emojis? (is it even possible to create this app?) 

•	The last technicality would be how to implement the capmojis in the texts. Our first thougts on how to go about it, is to simply have an emoji button; when pressed, it takes a picture of ones expression and “emotions” and fits it into the original emoji format. Then we have to get the camera and the message app to work together, and figure out wether to store some of the capmojis as the standards, or wether to always create new ones, deleting the old. 




