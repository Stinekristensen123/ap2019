







 What are the rules in your generative program and describe how your program performs over time. What have been generated beyond just the end product? 

I have created my generative program together with Sophie. At first we tried to figure out which simple rules we would like to implement in our program. By watching different generative art and going through their code, we suddenly found Langton’s ant. 
The code is running on a grid consisting of black and white cells. We identify one square as the “ant” that can travel in any of the four directions at each step it takes. There are only two simple rules in the code, for which move the ant will make. 
•	At a white square, turn 90° right, flip the color of the square, move forward one unit
•	At a black square, turn 90° left, flip the color of the square, move forward one unit
We have tried to bend and break the original rules, by changing the parameters of the visual effects such as colours and size. Since the whole concept of the program is based on the colours black and white, we decided to change these colours. It changed the visual experience. Furthermore we decided to create the background colour beneath function draw (). Due to the fact that function draw() is looping, the background colour is overdrawing the path of the ant. 

It can be discussed if this program should be seen as “strong” or “weak” generative art. I would categorize it as “weak” generative art, since it isn’t that random. It looks very random, but I definitely cannot say that the authorship is removed. Chris Langton has decided which random rules the program has implemented. But at least it is eternal, which is in my opinion enough to call it “generative art”. 

Link: https://cdn.staticaly.com/gl/Stinekristensen123/ap2019/raw/master/mini_Ex(new)/Mini_ex7/empty-example/index.html


