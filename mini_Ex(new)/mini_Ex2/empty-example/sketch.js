
//Neutral emoji

var cnv;
var d;
var g;
function setup() {
  cnv=createCanvas(600, 600);
	cnv.mouseMoved(changeSize);
	d=150;
	g=100;
	d_eye1=20;
	d_eye2=20;
}

function draw() {

	let c = color('#000000');
	let f= color('#A1F19C');
	background(color('#E7C3E8'));
	fill(f)
	ellipse(width / 2, height / 2, d, d);
	fill(c);
	eye1=ellipse(265, 280, d_eye1, d_eye1);
	eye2=ellipse(335, 280, d_eye2, d_eye2);
	mouth=ellipse(300,330,80,10)


}
//Denne funktion bruges til at få objektet til at bevæge sig
function mouseMoved() {
  g = g + 5;
  if (g > 255) {
    g = 0;
		}

}
//Denne funktion bruges til at ændre størrelsen på objektet
function changeSize() {
  d = d + 3;
  if (d > 300) {
    d = 150;
  }

}
