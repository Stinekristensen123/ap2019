Emoji 1:


[RUNME](https://glcdn.githack.com/Stinekristensen123/ap2019/raw/master/mini_exes/mini_ex2/index.html)

*Describe your program and what you have used and learned*

I have tried to design the most neutral and plain smiley I could think of. I have chosen the green skin colour, so that no one could identify himself or herself with it. Furthermore the size of the head will change, when the mouse moves anywhere on the canvas. – It can either be a thick or a thin smiley. Finally the gender of the smiley is neither a he nor a she. 
I have used and learned some new syntaxes: 
-function mouseMoved()
-function changeSize()
-Variables 


*What have you learned from the assigned reading*

From the assigned reading, I have learned that you have to consider the program carefully before it can be used in public. A program can easily affect the society and people can feel discriminated. Therefore it was important to me, to create an emoji that people could not identify them with.

















![ScreenShot](SBface.png)







Emoji 2:

I wanted to make an emoji that symbolized a feeling without using a smiley/a face. - Just like a heart often symbolizes love, I wanted to create an emoji that everyone would learn what symbolizes. Thereby no one would feel left out or discriminated. I have also learned to use some new syntaxes in this program. I have learned to use a curve and create a certain shape with variables. 
Unfortunately I was not able to opload my sketch.. By mistake I deleted the folder with this program. 




![ScreenShot](SBdrop.png)