
var paddle, ball, wallTop, wallBottom, wallLeft, wallRight;
var bricks;
var MAX_SPEED = 9;
var WALL_THICKNESS = 30;
var boldimg;

//loading ball image
function preload(){
  boldimg=loadImage("bold.png");

}

function setup() {
  createCanvas(800, 600);

  //Creating paddle
  paddle = createSprite(width/2, height-50, 100, 10);
  paddle.immovable = true;

  //Creating top so the ball cannot jump out
  wallTop = createSprite(width/2, -WALL_THICKNESS/2, width+WALL_THICKNESS*2, WALL_THICKNESS);
  wallTop.immovable = true;

  //Creating bottom so the ball cannot jump out
  wallBottom = createSprite(width/2, height+WALL_THICKNESS/2, width+WALL_THICKNESS*2, WALL_THICKNESS);
  wallBottom.immovable = true;

  //Creating the sides so the ball cannot jump out
  wallLeft = createSprite(-WALL_THICKNESS/2, height/2, WALL_THICKNESS, height);
  wallLeft.immovable = true;

  wallRight = createSprite(width+WALL_THICKNESS/2, height/2, WALL_THICKNESS, height);
  wallRight.immovable = true;



  //Creating ball using image
  ball = createSprite(width/2, height-200, 11, 11);
  ball.addImage(boldimg)
  ball.scale = 0.5;
  ball.maxSpeed = MAX_SPEED;
  paddle.shapeColor = ball.shapeColor = color(255, 255, 255);

}

function draw() {
  background('#99FFFF');

  //Making the paddle following the mouse
  paddle.position.x = constrain(mouseX, paddle.width/2, width-paddle.width/2);

  //Making the ball bounce randomly between the walls
  ball.bounce(wallTop);
  ball.bounce(wallBottom);
  ball.bounce(wallLeft);
  ball.bounce(wallRight);

  //Using if statement to make the ball recognize the paddle
  if(ball.bounce(paddle))
  {
    var swing = (ball.position.x-paddle.position.x)/3;
    ball.setSpeed(MAX_SPEED, ball.getDirection()+swing);
  }



  drawSprites();
}

function mousePressed() {
  if(ball.velocity.x == 0 && ball.velocity.y == 0)
    ball.setSpeed(MAX_SPEED, random(90-10, 90+10));
}
