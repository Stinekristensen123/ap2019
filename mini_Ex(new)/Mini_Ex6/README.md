[RUNME](https://cdn.staticaly.com/gl/Stinekristensen123/ap2019/raw/master/mini_Ex(new)/Mini_Ex6/empty-example/index.html)



![ScreenShot](Skærmbillede 2019-03-19 kl. 17.24.37.png)














My first thought going into this week’s mini_ex creation, was to make a simple and classic game. I didn’t want to create a complete new game, because I wanted to focus on learning how to use sprites and other features from the play library. 

After a short time I realised that I didn’t even have the skills to create a whole new game. I even had to copy a game and only change some of the factors in it. 
After reading about the play library and watching all sorts of different codes for classic games, I made my decision and chose a game called “Break out”. The game is using a ball and a paddle to bounce the ball. The ball has to collect some bricks. 
At first I tried to simply change some of numbers in random syntaxes, just to figure out what they were doing and how they worked. At last I added a sprite with an image of a football and deleted all the bricks. 

There is no concept behind my work, because tried to focus on understanding the new syntaxes and the new library. I found this mini_ex very complex, and I’m actually not happy with the result. But hopefully I will be able to use the play library soon. 




