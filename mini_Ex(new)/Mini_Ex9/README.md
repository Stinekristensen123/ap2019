Link: https://cdn.staticaly.com/gl/Stinekristensen123/ap2019/raw/master/mini_Ex(new)/Mini_Ex9/empty-example/index.html

This program is called “Weatherface” and is created by group 5; Mette, Sophie and Stine.

Before the working process started, we quickly agreed that we wanted to use data that was dynamic, fast changing and where the differences could be shown visually. We chose to implement the open weather API data. Weather data is something that most people can relate to and is widespread in society. Therefore we thought it would be interesting to work with and explore how we could change it in the program.
The program consists of a searchfield, where you can type in a name of a specific city. In the center of the canvas, two ellipses will occur and represent the weather of the city. The left ellipse depicts the temperature and the right ellipses depicts the humidity. The size of the ellipses will change depending on the data of the API. The temperature and the humidity is linked to our chosen API, and thereby the program illustrates the difference between these. The program should be illustrating a “weatherface”, where the two ellipses act as the eyes.

**The process**

It was a big challenge for our group to implement a program that utilizes web API. We saw the Shiftmann tutoring videos collectively. It was a tough process getting the API key to work in Atom, but when we finally got it working, we were ready to think creatively and come up with a concept. But first we had to understand the data the API gave us. It was actually easy to figure out which data our API contained, because of our understanding of parsing. When we parsed the data, the data was split in significative parts. It separated the data into different instructions, so it was easy to pick the data we wanted to use. The data was among other separated into groups called: “Weather” → “Main” (temperature, pressure, humidity)→ “Wind”(Speed, deg) → “Clouds”, etc. Our main idea was to get the data of what would be most interesting to know about the weather, and therefore we chose temperature and humidity.
Are thoughts behind the program is mainly about how much the weather affects us. Most people check the weather forecast everyday, chooses their holiday destination depending on weather, has a ‘favorite season’ and especially in the northern part of the world people suffer from winter depression, which is not common in countries where the sun shines the majority of the day and the temperatures are high. All of these phenomena inspired us to do a program where the weather data affects a smiley-like-face.This idea came natural to us as we have a lot of focus on the previous emoji-theme because we want our final project to be about this theme.

**Significance of API in digital culture**

API is making it possible to share external data and connect it with our own. API’s are everywhere in our personal lives,- when we are watching Youtube, posting things on Facebook, etc. They actually enable our digital lives. Furthermore they make it possible for a programmer, to use it and create something dynamic and “live”.

**Try to formulate a question in relation to web APIs or querying processes that you want to investigate further when you have more time**

To really try and understand what an API does, is a complex matter. It’s like we are all interconnected, sharing data and loaning from all the devices we can get access to.
In our group, we were very fascinated by generative art, and how randomness can tricker the human mind.
Therefore we found it interesting to contemplate about what API’s bring to generative art, and how the random factors are more like pseudo-random data we loan from our connections with others.