var weather;
var api = 'http://api.openweathermap.org/data/2.5/weather?q=';
var apiKey = '&APPID=487daf1d323377e1e99b4de7324a69b7';
var units = '&units=metric';


function preload () {
  clouds = loadImage('Clouds.png');





}


function setup() {
  createCanvas(windowWidth,windowHeight);

  var button = select('#submit');
  button.mousePressed(weatherAsk);
  input = select ('#city');
}




function weatherAsk () {
var url = api + input.value() + apiKey + units;
loadJSON(url, gotData);

}




function gotData(data){
  //println(data);
weather=data;

}



function draw () {

  image(clouds,250,0);



  if(weather) {

    //let c= color(#FFFF33);
    ellipse(500,300, (weather.main.temp)+15, (weather.main.temp)+15); //to the left


    ellipse(800, 300, (weather.main.humidity)+15, (weather.main.humidity)+15); //to the right


  }

  }
